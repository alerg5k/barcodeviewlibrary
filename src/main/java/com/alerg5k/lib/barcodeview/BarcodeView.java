package com.alerg5k.lib.barcodeview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;

/**
 * TODO: document your custom view class.
 */
public class BarcodeView extends View {
    private String mBarcodeText = "000000"; // TODO: use a default from R.string...
    private Drawable mExampleDrawable;

    private int mBarSize;
    private int mBarsCount;
    private Paint mColorWhite, mColorBlack;

    private ArrayList<Integer> mBars;
    private int paddingLeft;
    private int paddingTop;
    private int paddingRight;
    private int paddingBottom;
    private int contentWidth;
    private int contentHeight;

    private static List<Character> mCode128SetB = asList(' ', '!', '"', '#', '$', '%', '&', '\'',
            '(', ')', '*', '+', ',', '-', '.', '/', '0', '1', '2', '3', '4', '5', '6', '7', '8',
            '9', ':', ';', '<', '=', '>', '?', '@', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I',
            'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
            '[', '\\', ']', '^', '_', '`', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k',
            'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '{', '|',
            '}', '~', (char)127);
    private static List<Character> mCode128SetA = asList(' ', '!', '"', '#', '$', '%', '&', '\'',
            '(', ')', '*', '+', ',', '-', '.', '/', '0', '1', '2', '3', '4', '5', '6', '7', '8',
            '9', ':', ';', '<', '=', '>', '?', '@', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I',
            'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
            '[', '\\', ']', '^', '_', (char)0, (char)1, (char)2, (char)3, (char)4, (char)5, (char)6,
            (char)7, (char)8, (char)9, (char)10, (char)11, (char)12, (char)13, (char)14, (char)15,
            (char)16, (char)17, (char)18, (char)19, (char)20, (char)21, (char)22, (char)23,
            (char)24, (char)25, (char)26, (char)27, (char)28, (char)29, (char)30, (char)31);

    private static Character startA = (char)103;
    private static Character startB = (char)104;
    private static Character startC = (char)105;
    private static Character codeA = (char)101;
    private static Character codeB = (char)100;
    private static Character codeC = (char)99;
    private static Character shiftAB = (char)98;

    private static SparseArray<String> mCode128Values = new SparseArray<String>() {
        {
            append(0, "212222");
            append(1, "222122");
            append(2, "222221");
            append(3, "121223");
            append(4, "121322");
            append(5, "131222");
            append(6, "122213");
            append(7, "122312");
            append(8, "132212");
            append(9, "221213");
            append(10, "221312");
            append(11, "231212");
            append(12, "112232");
            append(13, "122132");
            append(14, "122231");
            append(15, "113222");
            append(16, "123122");
            append(17, "123221");
            append(18, "223211");
            append(19, "221132");
            append(20, "221231");
            append(21, "213212");
            append(22, "223112");
            append(23, "312131");
            append(24, "311222");
            append(25, "321122");
            append(26, "321221");
            append(27, "312212");
            append(28, "322112");
            append(29, "322211");
            append(30, "212123");
            append(31, "212321");
            append(32, "232121");
            append(33, "111323");
            append(34, "131123");
            append(35, "131321");
            append(36, "112313");
            append(37, "132113");
            append(38, "132311");
            append(39, "211313");
            append(40, "231113");
            append(41, "231311");
            append(42, "112133");
            append(43, "112331");
            append(44, "132131");
            append(45, "113123");
            append(46, "113321");
            append(47, "133121");
            append(48, "313121");
            append(49, "211331");
            append(50, "231131");
            append(51, "213113");
            append(52, "213311");
            append(53, "213131");
            append(54, "311123");
            append(55, "311321");
            append(56, "331121");
            append(57, "312113");
            append(58, "312311");
            append(59, "332111");
            append(60, "314111");
            append(61, "221411");
            append(62, "431111");
            append(63, "111224");
            append(64, "111422");
            append(65, "121124");
            append(66, "121421");
            append(67, "141122");
            append(68, "141221");
            append(69, "112214");
            append(70, "112412");
            append(71, "122114");
            append(72, "122411");
            append(73, "142112");
            append(74, "142211");
            append(75, "241211");
            append(76, "221114");
            append(77, "413111");
            append(78, "241112");
            append(79, "134111");
            append(80, "111242");
            append(81, "121142");
            append(82, "121241");
            append(83, "114212");
            append(84, "124112");
            append(85, "124211");
            append(86, "411212");
            append(87, "421112");
            append(88, "421211");
            append(89, "212141");
            append(90, "214121");
            append(91, "412121");
            append(92, "111143");
            append(93, "111341");
            append(94, "131141");
            append(95, "114113");
            append(96, "114311");
            append(97, "411113");
            append(98, "411311");
            append(99, "113141");
            append(100, "114131");
            append(101, "311141");
            append(102, "411131");
            append(103, "211412");
            append(104, "211214");
            append(105, "211232");
            append(106, "2331112");
        }
    };

    public BarcodeView(Context context) {
        super(context);
        init(null, 0);
    }

    public BarcodeView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }

    public BarcodeView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs, defStyle);
    }

    private void init(AttributeSet attrs, int defStyle) {
        // Load attributes
        final TypedArray a = getContext().obtainStyledAttributes(
                attrs, R.styleable.BarcodeView, defStyle, 0);

        mBarcodeText = a.getString(
                R.styleable.BarcodeView_barcodeText);

        mBars = new ArrayList<Integer>();
        mBars.add(105);
        mBars.add(0);
        mBars.add(6);
        mBars.add(67);
        mBars.add(9);
        mBars.add(106);

        mColorWhite = new Paint(0);
        mColorWhite.setColor(0xffffffff);

        mColorBlack = new Paint(0);
        mColorBlack.setColor(0xff000000);

        a.recycle();

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        // Draw the barcode
        canvas.drawRect(0, 0, this.getWidth(), this.getHeight(), mColorWhite);
        drawBarCode(canvas);
    }

    private void drawBarCode(Canvas canvas) {
        int left = 0;
        //calculate remaining space
        int rem = (this.getWidth() - mBarSize * mBarsCount) / 2;
        left += rem;
        //calculate quiet zone left
        left += mBarSize * 10;

        for (int i : mBars) {
            String val = mCode128Values.get(i);
            for (int c = 0; c < val.length(); c++){
                int w  = val.charAt(c) - '0';
                if (c % 2 == 0) {
                    canvas.drawRect(left, 0, left + w * mBarSize, this.getHeight(), mColorBlack);
                } else {
                    canvas.drawRect(left, 0, left + w * mBarSize, this.getHeight(), mColorWhite);
                }
                left += w * mBarSize;
            }
        }
    }

    private int countBars() {
        int s = 0;
        for (int i : mBars) {
            String val = mCode128Values.get(i);
            Log.d("AAA", "val: " + val + " : " + String.valueOf(i));
            for (int c = 0; c < val.length(); c++){
                s += (val.charAt(c) - '0');
            }
        }
        s += 20; //quiet zone, 2*10
        Log.d("AAA", "count s: " + String.valueOf(s));
        return s;
    }

    @Override
    protected void onSizeChanged (int w, int h, int oldw, int oldh) {
        paddingLeft = getPaddingLeft();
        paddingTop = getPaddingTop();
        paddingRight = getPaddingRight();
        paddingBottom = getPaddingBottom();

        contentWidth = w - paddingLeft - paddingRight;
        contentHeight = h - paddingTop - paddingBottom;

        calculateBarSize();
    }

    private void calculateBarSize() {
        mBarsCount = countBars();
        Log.d("AAA", "onSizeChanged barscount: " + String.valueOf(mBarsCount) + ", " +
                "new width: " + contentWidth);
        mBarSize = contentWidth / mBarsCount;
    }

    /**
     * Gets the example string attribute value.
     *
     * @return The example string attribute value.
     */
    public String getBarcodeText() {
        return mBarcodeText;
    }

    private boolean checkText(String s) {
        boolean ret = true;
        for (int i=0; i<s.length(); i++) {
            if (!Character.isDigit(s.charAt(i))) {
                ret = false;
                break;
            }
        }
        return ret;
    }

    private int calculateCheckChar() {
        int s = 105; //can be 103 or 104
        int pos = 1;
        for (int i:mBars) {
            if (i < 103) {
                s += i*pos;
                pos++;
            }
        }
        Log.d("AAA", "sum for check: " + String.valueOf(s));
        return s % 103;
    }

    private void calculateBars2() {
        int currentPosition = 0;
        mBars.clear();
        int start = calculateStartChar(mBarcodeText);
        int code = 'A' - 103 + start; //'A', 'B' or 'C'

        Log.d("AAA", "start: " + String.valueOf(start));
        mBars.add(start);
        //2. if start char C is used and the data begins with an odd number of numeric characters
        //insert a code StartA or StartB before the last digit following rules 1c and 1d
        if (start == 105) {
            int digits = countNextDigits(mBarcodeText, currentPosition);
            Log.d("AAA", "digits2: " + String.valueOf(digits));
            for (int i=currentPosition; i<digits-1; i+=2) {
                //digits-1 is making sure we don't pass over the correct number of digits
                String ss = mBarcodeText.substring(i, i+2);
                Log.d("AAA", "ss: " + ss);
                mBars.add(Integer.parseInt(ss));
                currentPosition += 2;
            }
            //at this point we parse all the needed digits, if one left will be treated as a
            //codeA or codeB character
            //check if we have anything left and add a code change character
            if ((digits < mBarcodeText.length()) || (digits % 2 != 0)) {
                if (checkLowerCaseBeforeControl(mBarcodeText, currentPosition, false)) {
                    mBars.add(((int) codeA.charValue()));
                    code = 'A';
                    Log.d("AAA", "codeA");
                } else {
                    mBars.add(((int) codeB.charValue()));
                    code = 'B';
                    Log.d("AAA", "codeB");
                }
                //check if we still have a digit to parse
                if (digits % 2 != 0) {
                    //in both codeA and codeB digits start at position 16
                    mBars.add((int) mBarcodeText.charAt(currentPosition) - '0' + 16);
                    currentPosition++;
                    Log.d("AAA", "last digit" + String.valueOf(mBars.get(mBars.size() - 1)));
                }
            }
        }

        //right now we are either in codeA or codeB or have finished parsing (if all numeric)
        while (currentPosition < mBarcodeText.length()) {
            //3. if 4 or more numeric chars occur together when in codeA or codeB
            int digits = countNextDigits(mBarcodeText, currentPosition);
            Log.d("AAA", "digits3: " + String.valueOf(digits));
            if ((digits >= 4) && ((code == 'A') || (code == 'B'))) {
                //3a. if its an even number of digits insert codeC start before first numeric digit
                //3b. if its an odd number of digits insert codeC start immediately after the first
                // digit
                if (digits % 2 == 0) {
                    mBars.add((int) startC.charValue());
                } else {
                    mBars.add((int) mBarcodeText.charAt(currentPosition) + 16);
                    currentPosition++;
                    digits--;
                    mBars.add((int) startC.charValue());
                }
                for (; digits > 0; digits -= 2) {
                    String ss = mBarcodeText.substring(currentPosition, currentPosition+2);
                    Log.d("AAA", "ss3: " + ss);
                    mBars.add(Integer.parseInt(ss));
                    currentPosition += 2;
                }
            }

            //4. when in code B and an ASCII control char occurs
            if (code == 'B') {
                if (isControlCharA(mBarcodeText.charAt(currentPosition))) {
                    //4a. if following that char a lower case occurs before another control char
                    // insert a shift character before the control char
                    if (checkLowerCaseBeforeControl(mBarcodeText, currentPosition + 1, true)) {
                        mBars.add((int) shiftAB.charValue());
                    } else {
                        //4b. otherwise insert a code A char before the control char
                        mBars.add((int) codeA.charValue());
                        code = 'A';
                    }
                    mBars.add(getControlCodeA(mBarcodeText.charAt(currentPosition))); //control chars start at 64A
                    currentPosition++;
                } else {
                    //add code B char
                    mBars.add(getCodeB(mBarcodeText.charAt(currentPosition)));
                    currentPosition++;
                }
            }

            //5. when in code A and a lower case char occurs
            if (code == 'A') {
                if (isLowerCaseB(mBarcodeText.charAt(currentPosition))) {
                    //5a. if following that char a control char occurs before another lower case
                    // insert a shift character before the lower case char
                    if (checkLowerCaseBeforeControl(mBarcodeText, currentPosition + 1, false)) {
                        mBars.add((int) shiftAB.charValue());
                    } else {
                        //5b. otherwise insert a code B char before the lower case char
                        mBars.add((int) codeB.charValue());
                        code = 'B';
                    }
                    mBars.add(getLowerCaseCodeB(mBarcodeText.charAt(currentPosition)));
                    currentPosition++;
                } else {
                    //add code A char
                    mBars.add(getCodeA(mBarcodeText.charAt(currentPosition)));
                    currentPosition++;
                }
            }

            //6. when in code C and a non-numeric char occurs insert a codeA or codeB char before
            // the character following rules 1c and 1d (see calculateStartChar)
            if ((code == 'C') && (!Character.isDigit(mBarcodeText.charAt(currentPosition)))) {
                if (checkLowerCaseBeforeControl(mBarcodeText, currentPosition, false)) {
                    //1c if an ASCII control char  occurs in the data before any lower case char use
                    //start char A
                    mBars.add((int) codeA.charValue());
                    code = 'A';
                } else {
                    //1d otherwise use start char B
                    mBars.add((int) codeB.charValue());
                    code = 'B';
                }
            }
        }


        //add check char
        int checkChar = calculateCheckChar();
        Log.d("AAA", "checkchar: " + String.valueOf(checkChar));
        mBars.add(checkChar);

        mBars.add(106);

    }

    private void calculateBars() {
        mBars.clear();
        mBars.add(105); //can be 103 or 104

        for(int i=0; i<mBarcodeText.length(); i+=2) {
            String ss = mBarcodeText.substring(i, i+2);
            mBars.add(Integer.parseInt(ss));
            Log.d("AAA", "substring: " + ss);
        }

        int checkChar = calculateCheckChar();
        Log.d("AAA", "checkchar: " + String.valueOf(checkChar));
        mBars.add(checkChar);

        mBars.add(106);

    }

    private int getCodeB(char c) {
        int pos = mCode128SetB.indexOf(c);
        return pos;
    }

    private int getCodeA(char c) {
        int pos = mCode128SetA.indexOf(c);
        return pos;
    }

    private int getLowerCaseCodeB(char c) {
        //lower case start at 64, but ASCII code at 96
        return c - 32;
    }

    private int getControlCodeA(char c) {
        //control start at 64, but ASCII code at 0
        return c + 64;
    }

    private boolean isLowerCaseB(char c) {
        return (c >= 96) && (c <=127);
    }

    private boolean isControlCharA(char c) {
        return (c >= 0) && (c <=31);
    }

    /**
     * returns the number of consecutive digits starting with position p
     */
    private int countNextDigits(String barcode, int p) {
        int c = 0;
        while (((p+c) < barcode.length()) && (Character.isDigit(barcode.charAt(p+c)))) {
            c++;
        }
        return c;
    }

    /**
     *  check if next N characters from the string starting with position p are digits
     */
    private boolean checkNextDigits(String barcode, int n, int p) {
        boolean ret = true;
        for (int i=p; i<p+n; i++) {
            if (!Character.isDigit(barcode.charAt(i))) {
                ret = false;
                break;
            }
        }
        return ret;
    }

    /**
     * checks if a lower case char appears in the string before a ASCII control char, starting with
     * position p. Boolean check reverse the logic (control char before lower case)
     */
    private boolean checkLowerCaseBeforeControl(String barcode, int p, boolean check) {
        boolean ret = !check;
        for (int i=p; i<barcode.length(); i++) {
            if (Character.isLowerCase(barcode.charAt(i))) {
                ret = check;
                break;
            } else if (Character.isISOControl(barcode.charAt(i))) {
                break;
            }
        }
        return ret;
    }

    /**
     *
     */
    private int calculateStartChar(String barcode) {
        Character ret = startB;
        if ((barcode.length() == 2) && (checkNextDigits(barcode, 2, 0))) {
            //1a if the data consist of 2 digits  use start char C
            ret = startC;
        } else if (checkNextDigits(barcode, 4, 0)) {
            //1b if data begins with more than 4 digits use start char C
            ret = startC;
        } else if (checkLowerCaseBeforeControl(barcode, 0, false)) {
            //1c if an ASCII control char  occurs in the data before any lower case char use
            //start char A
            ret = startA;
        }
        //1d otherwise return start char B
        return ret.charValue();
    }

    /**
     * Sets the view's example string attribute value. In the example view, this string
     * is the text to draw.
     *
     * @param barcode The barcode string to draw.
     */
    public void setBarcodeText(String barcode) {
        Log.d("AAA", "setBarcodeText");
        /*if (barcode.length() % 2 != 0) {
            barcode = "0" + barcode;
        }
        Log.d("AAA", "barcode: " + barcode);
        if (checkText(barcode)) {
            Log.d("AAA", "barcode ok");
            mBarcodeText = barcode;
            calculateBars();
            invalidate();
        }*/
        Log.d("AAA", "barcode ok");
        mBarcodeText = barcode;
        calculateBars2();
        invalidate();

    }

}
